# Mini App Speech Bubble JSON
## 4210181024 Frederiko Adrian R B

![Speech Bubble gif](4210181024_Frederiko Adrian.gif)

## Code Description
1. As the program is running, the data in JSON form at the following link [URL](https://5e510330f2c0d300147c034c.mockapi.io/users) will be downloaded.
2. If the network does not experience an error, then the data is converted into strings and arranged to be more organized into the ClassOrang class array.
3. This is done using the Unity's Wrapper and JsonUtility classes.
4. A function is created to be able to display data in sentences that have been stored in the class array at the desired index, which is 24.
5. This function is called when the players press the speech bubble so that the sentence and the data appear.

## Code
- [JSONResponse.cs](Chat Bubble/Assets/Script/JSONResponse.cs)
