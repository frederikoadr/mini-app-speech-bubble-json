﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class JSONResponse : MonoBehaviour
{
    [Serializable]
    public class ClassOrang
    {
        public string id;
        public string name;
        public string avatar;
        public string email;
    }
    public ClassOrang[] orang;
    [SerializeField] Text textSpeech;

    private string url = "https://5e510330f2c0d300147c034c.mockapi.io/users"; //Data JSON

    void Start()
    {
        StartCoroutine(ReceiveResponse());
    }

    private IEnumerator ReceiveResponse()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();
        
        if(www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            string JSONtext = "{\"Items\":" + www.downloadHandler.text + "}";
            //MyClass[] orang = JsonUtility.FromJson<MyClass[]>(JSONtext);
            orang = JsonHelper.FromJson<ClassOrang>(JSONtext);
            Debug.Log("JSON Data Retrieved");
        }
    }
    public void Introduce()
    {
        textSpeech.text = "Hi! My name is " + orang[24].name + ". You can contact me at " + orang[24].email;
    }
}
public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}